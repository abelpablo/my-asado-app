package com.example.mordor.asado_app.clases;

import android.content.Context;

import com.example.mordor.asado_app.database.DatabaseHelper;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

public class PeopleFactory {

    private static PeopleFactory instance;
    private Dao<People, Integer> itemDao;

    private PeopleFactory(Context context) {
        OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(context, DatabaseHelper.class);

        try {
            itemDao = helper.getDao(People.class);
        } catch (SQLException e) {
            // Boo hoo
        }
    }

    public static PeopleFactory getInstance(Context context) {
        if (instance == null) {
            instance = new PeopleFactory(context);
        }

        return instance;
    }

    public List<People> getPeopleList() throws SQLException {
        return itemDao.queryForAll();
    }

    public boolean addPeople(People newItem) {
        try {
            itemDao.create(newItem);
        } catch (SQLException e) {
            return false;
        }

        return true;
    }

    public boolean removeItem(People toDelete) {
        try {
            itemDao.delete(toDelete);
        } catch (SQLException e) {
            return false;
        }

        return true;
    }
}
