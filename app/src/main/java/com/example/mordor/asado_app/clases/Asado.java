package com.example.mordor.asado_app.clases;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Mordor on 28/07/2016.
 */
public class Asado {
    private Date fechaAsado;
    private ArrayList<People> invitados;
    private boolean status = false;
    private long costoTotal = 0;

    public long getCostoTotal() {
        return costoTotal;
    }

    public void setCostoTotal(long costoTotal) {
        this.costoTotal = costoTotal;
    }


    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getFechaAsado() {
        return fechaAsado;
    }

    public void setFechaAsado(Date fechaAsado) {
        this.fechaAsado = fechaAsado;
    }

    public ArrayList<People> getInvitados() {
        return invitados;
    }

    public void setInvitados(ArrayList<People> invitados) {
        this.invitados = invitados;
    }
}
