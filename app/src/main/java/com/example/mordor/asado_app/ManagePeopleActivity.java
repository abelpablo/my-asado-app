package com.example.mordor.asado_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ListView;

import com.example.mordor.asado_app.clases.People;
import com.example.mordor.asado_app.clases.PeopleAdapter;
import com.example.mordor.asado_app.clases.PeopleFactory;

import java.util.List;

public class ManagePeopleActivity extends AppCompatActivity {

    private ListView peopleList;
    private PeopleFactory peopleFactory;
    private PeopleAdapter peopleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_people);
        peopleList = (ListView) findViewById(R.id.ListOfPeople);
        peopleFactory = PeopleFactory.getInstance(this);

        try {
            peopleAdapter = new PeopleAdapter(peopleFactory.getPeopleList());
            peopleList.setAdapter(peopleAdapter);
        } catch (Exception none) {
            Log.e(ManagePeopleActivity.class.toString(), "Something bad happened");
        }
    }

    public void onClickAddPeople(View v) throws Exception {
        Intent newView = new Intent(this, AddPeople.class);
        startActivity(newView);
    }

    @Override
    public void onResume () {
        super.onResume();
        PeopleFactory pf = PeopleFactory.getInstance(getBaseContext());
        try {
            List<People> peoples = pf.getPeopleList();
        } catch (Exception e) {
            // shupala
        }

        CheckBox ck = new CheckBox(getBaseContext());

    }
}
