package com.example.mordor.asado_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.mordor.asado_app.clases.People;
import com.example.mordor.asado_app.clases.PeopleFactory;

public class AddPeople extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_people);
    }

    public void saveNewPeople(View view){
        EditText people_name = (EditText) findViewById(R.id.peopleName);
        EditText people_email = (EditText) findViewById(R.id.peopleEmail);
        CheckBox eater = (CheckBox) findViewById(R.id.peopleEater);
        CheckBox buyer = (CheckBox) findViewById(R.id.peopleBuyer);
        CheckBox asador = (CheckBox) findViewById(R.id.peopleAsador);

        People newPeople = new People();
        newPeople.setMail(people_email.getText().toString());
        newPeople.setName(people_name.getText().toString());
        newPeople.setRolEater(eater.isChecked());
        newPeople.setRolBuyer(buyer.isChecked());
        newPeople.setRolAsador(asador.isChecked());

        PeopleFactory pf = PeopleFactory.getInstance(view.getContext());

        pf.addPeople(newPeople);
        onBackPressed();

    }
}
