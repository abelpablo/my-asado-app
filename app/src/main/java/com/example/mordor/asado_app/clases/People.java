package com.example.mordor.asado_app.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Mordor on 28/07/2016.
 */
@DatabaseTable(tableName = "people")
public class People {

    @DatabaseField
    private String name;
    @DatabaseField
    private String mail;
    @DatabaseField
    private boolean rolAsador;
    @DatabaseField
    private boolean rolEater;
    @DatabaseField
    private boolean rolBuyer;

    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
    private int MyId;

    public int getMyId() {
        return MyId;
    }

    public void setMyId(int myId) {
        MyId = myId;
    }

    public boolean isRolAsador() {
        return rolAsador;
    }

    public void setRolAsador(boolean rolAsador) {
        this.rolAsador = rolAsador;
    }

    public boolean isRolEater() {
        return rolEater;
    }

    public void setRolEater(boolean rolEater) {
        this.rolEater = rolEater;
    }

    public boolean isRolBuyer() {
        return rolBuyer;
    }

    public void setRolBuyer(boolean rolBuyer) {
        this.rolBuyer = rolBuyer;
    }

    public String getName() {
        return name;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }
}
