package com.example.mordor.asado_app.clases;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


import com.example.mordor.asado_app.R;

import org.w3c.dom.Text;

import java.util.List;

public class PeopleAdapter extends BaseAdapter {

    private List<People> peoples;

    public PeopleAdapter(List<People> items) {
        this.peoples = items;
    }

    @Override
    public int getCount() {
        return peoples.size();
    }

    @Override
    public People getItem(int position) {
        return peoples.get(position);
    }

    @Override
    public long getItemId(int position) {
        return peoples.get(position).getMyId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View currentView = null;

        if (convertView == null) {
            currentView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_people, parent, false);
        } else {
            currentView = convertView;
        }

        People item = getItem(position);

        ((TextView) currentView.findViewById(R.id.peopleName)).setText(item.getName());
        ((TextView) currentView.findViewById(R.id.peopleEmail)).setText(item.getMail());

        LinearLayout roles = (LinearLayout) currentView.findViewById(R.id.peopleRoles);

        TextView elem;

        if (item.isRolAsador()) {
            elem = new TextView(currentView.getContext());
            elem.setId(item.getMyId() + 1000);
            elem.setText("Es asador");
            roles.addView(elem);
        } else {
            elem = (TextView) currentView.findViewById(item.getMyId() + 1000);
            if (elem != null) {
                roles.removeView(elem);
            }
        }

        if (item.isRolBuyer()) {
            elem = new TextView(currentView.getContext());
            elem.setId(item.getMyId() + 1001);
            elem.setText("Es comprador");
            roles.addView(elem);
        } else {
            elem = (TextView) currentView.findViewById(item.getMyId() + 1001);
            if (elem != null) {
                roles.removeView(elem);
            }
        }

        if (item.isRolEater()) {
            elem = new TextView(currentView.getContext());
            elem.setId(item.getMyId() + 1002);
            elem.setText("Es comedor");
            roles.addView(elem);
        } else {
            elem = (TextView) currentView.findViewById(item.getMyId() + 1002);
            if (elem != null) {
                roles.removeView(elem);
            }
        }

        return currentView;
    }

    public void setNewElements(List<People> newElements) {
        peoples = newElements;
    }
}
